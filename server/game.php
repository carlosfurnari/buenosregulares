<?php

require_once 'DB_Functions.php';
$db = new DB_Functions();


$response = array();


if (isset($_GET['action'])) {

    if ($_GET["action"] == "createGame") {

        // receiving the post params
        $game_name = $_GET['game_name'];
        $owner = $_GET["owner"];
        $number = $_GET["number"];

        if ($db->gameIsAvailableByName($game_name)) {
            $result = $db->createGame($game_name, $owner, $number);
            if ($result) {
                $response["status"] = "Ok";
                $response["game_id"] = $result["game_id"];
                echo json_encode($response);
                exit;
            }

        } else {
            $response["status"] = "Error";
            $response["error_msg"] = "Nombre en uso";
            echo json_encode($response);
            exit;
        }

    }

    if ($_GET["action"] == "getGames") {

        // receiving the post params

        $games = $db->gamesWaiting();
        $response["status"] = "Ok";
        $response["games"] = $games;
        echo json_encode($response);
        exit;

    }

    if ($_GET["action"] == "joinGame") {

        // receiving the post params
        $game_id = $_GET['game_id'];
        $player = $_GET["player"];
        $number = $_GET["number"];

        if ($db->gameIsAvailableById($game_id)) {
            $result = $db->addPlayer($game_id, $player, $number);
            if ($result) {

                $response["status"] = "Ok";
                echo json_encode($response);
                exit;
            }else {
                $response["status"] = "Error";
                $response["error_msg"] = "Error al unir al jugador a la partida";
                echo json_encode($response);
                exit;
            }

        } else {
            $response["status"] = "Error";
            $response["error_msg"] = "Error al unir al jugador a la partida";
            echo json_encode($response);
            exit;
        }

    }

    if ($_GET["action"] == "isGameReady") {

        // receiving the post params
        $game_id = $_GET['game_id'];


        if ($db->gameIsReady($game_id)) {

            $response["status"] = "Ok";
            $response["ready"] = true;
            echo json_encode($response);
            exit;
        } else {
            $response["status"] = "Ok";
            $response["ready"] = false;
            echo json_encode($response);
            exit;
        }

    }

    if ($_GET["action"] == "isMyTurn") {

        // receiving the post params
        $game_id = $_GET['game_id'];
        $player = $_GET["player"];

        if (!$db->isGameOver($game_id)) {
            $last_player = $db->lastPlayer($game_id);
            if (strcmp($last_player, $player) == 0) {
                $response["status"] = "Ok";
                $response["turn"] = false;
                echo json_encode($response);
                exit;
            } else {
                $response["status"] = "Ok";
                $response["turn"] = true;
                $result = $db->getLastTurn($game_id);

                if ($result) {
                    $response["corrects"] = $result["corrects"];
                    $response["regulars"] = $result["regulars"];
                    $response["number"] = $result["number"];
                    $response["player"] = $result["player"];
                    echo json_encode($response);
                    exit;

                }
                echo json_encode($response);
                exit;
            }

        } else {
            $response["status"] = "Fin";
            $response["error_msg"] = "El juego ha terminado";
            echo json_encode($response);
            exit;
        }

    }

    if ($_GET["action"] == "sendNumber") {

        // receiving the post params
        $game_id = $_GET['game_id'];
        $player = $_GET["player"];
        $risked_number = $_GET["number"];
        $cr = array();

        if (!$db->isGameOver($game_id)) {
            $real_number=$db->getOpponentNumber($game_id,$player);
            $cr = checkCorrects($risked_number,$real_number);

            $result = $db->saveTurn($game_id, $player, $risked_number, $cr["corrects"], $cr["regulars"]);
            if ($result) {
                $response["status"] = "Ok";
                $response["corrects"] = $cr["corrects"];
                $response["regulars"] = $cr["regulars"];
                if ($cr["corrects"] == 4) {
                    $result = $db->gameWinner($game_id, $player);
                }
                echo json_encode($response);
                exit;
            }

        } else {
            $response["status"] = "Error";
            $response["error_msg"] = "El juego ha terminado";
            echo json_encode($response);
            exit;
        }
    }

    if ($_GET["action"] == "getLastTurnData") {

        // receiving the post params
        $game_id = $_GET['game_id'];

        $cr = array();

        if (!$db->isGameOver($game_id)) {

            $result = $db->getLastTurn($game_id);

            if ($result) {
                $response["status"] = "Ok";
                $response["corrects"] = $result["corrects"];
                $response["regulars"] = $result["regulars"];
                $response["number"] = $result["number"];
                $response["player"] = $result["player"];
                echo json_encode($response);
                exit;

            }else{
                $response["status"] = "Error";
                $response["game_over"]=false;
                $response["error_msg"] = "No hay turnos";
                echo json_encode($response);
                exit;

            }


        } else {
            $response["status"] = "Error";
            $response["game_over"]=true;
            $response["error_msg"] = "El juego ha terminado";
            echo json_encode($response);
            exit;
        }

    }

} else {
    $response["status"] = "Error";
    $response["error_msg"] = "Required parameters are missing!";
    echo json_encode($response);
    exit;
}



function  checkCorrects($real_number,$risked_number){

    $risked_number=explode(",",$risked_number);
    $real_number=explode(",",$real_number);
    $result=array("corrects"=>0,"regulars"=>0);

    for($i=0;$i<4;$i++){
        for($j=0;$j<4;$j++){
            if($risked_number[$i]==$real_number[$j]){
                if($i==$j){
                    $result["corrects"]++;
                }else{
                    $result["regulars"]++;
                }
            }
        }
    }
    return $result;
}

?>