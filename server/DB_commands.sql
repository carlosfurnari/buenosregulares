/*
* CREACION DE TABLAS 
*
*/

/* CREACION TABLA DE JUEGOS DISPONIBLES */

CREATE TABLE `br_db`.`available_games_br` ( `game_id` INT NOT NULL AUTO_INCREMENT , `game_name` VARCHAR(25) NOT NULL , `owner` VARCHAR(25) NOT NULL , `number` CHAR(7) NOT NULL , PRIMARY KEY (`game_id`), UNIQUE (`game_name`)) ENGINE = InnoDB CHARSET=utf8 COLLATE utf8_spanish_ci;

/* CREACION TABLA DE JUEGOS */

CREATE TABLE `br_db`.`games_br` ( `game_id` INT NOT NULL AUTO_INCREMENT , `game_name` VARCHAR(25) NOT NULL , `player_1` VARCHAR(25) NOT NULL , `player_2` VARCHAR(25) NOT NULL , `number_1` CHAR(7) NOT NULL , `number_2` CHAR(7) NOT NULL , `last_turn` INT NOT NULL , `winner` VARCHAR(25) NOT NULL , PRIMARY KEY (`game_id`), UNIQUE (`game_name`)) ENGINE = InnoDB CHARSET=utf8 COLLATE utf8_spanish_ci;

/* CREACION TABLA DE TURNOS JUGADOS */

CREATE TABLE `br_db`.`turns_br` ( `turn_id` INT NOT NULL AUTO_INCREMENT , `game_id` INT NOT NULL , `player` VARCHAR(25) NOT NULL , `number` CHAR(7) NOT NULL , `correct_numbers` INT NOT NULL , `regular_numbers` INT NOT NULL , PRIMARY KEY (`turn_id`)) ENGINE = InnoDB CHARSET=utf8 COLLATE utf8_spanish_ci;

/* CREACION TABLA DE USUARIOS */

CREATE TABLE `br_db`.`users_br` ( `id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(25) NOT NULL , PRIMARY KEY (`id`)) EENGINE = InnoDB CHARSET=utf8 COLLATE utf8_spanish_ci;
