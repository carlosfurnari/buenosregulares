<?php
 
require_once 'DB_Functions.php';
$db = new DB_Functions();

// json response array
$response = array();
 
if (isset($_GET['user'])) {
 
    // receiving the post params
    $user_name= $_GET['user'];
    
    // check if user is already existed with the same email
    if ($db->userExists($user_name)) {
        // user already existed
        $response["status"] = "Error";
        $response["error_msg"] = "User already existed with " . $user_name;
        echo json_encode($response);
    } else {
        // create a new user
        $user = $db->storeUser($user_name);
        if ($user) {
            // user stored successfully
            $response["status"] = "Ok";
            $response["user"] = $user_name;
            echo json_encode($response);
        } else {
            // user failed to store
            $response["status"] = "Error";
            $response["user"] = $user_name;
            $response["error_msg"] = "Unknown error occurred in registration!";
            echo json_encode($response);
        }
    }
} else {

    $response["status"] = "Error";
    $response["error_msg"] = "Required parameter (name) is missing!";
    echo json_encode($response);
}
exit;
?>
