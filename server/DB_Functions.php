<?php

class DB_Functions {

    private $conn;

    // constructor
    function __construct() {
        require_once 'DB_Connect.php';
        // connecting to database
        $db = new Db_Connect();
        $this->conn = $db->connect();
    }

    // destructor
    function __destruct() {

    }

    /******* USER *******/

    /**
     * Storing new user
     * returns user details
     */
    public function storeUser($user_name) {


        $stmt = $this->conn->prepare("INSERT INTO users_br(name) VALUES(?)");
        $stmt->bind_param("s",$user_name);
        $result = $stmt->execute();
        $stmt->close();

        // check for successful store
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check user is existed or not
     */
    public function userExists($name) {
        $stmt = $this->conn->prepare("SELECT name from users_br WHERE name = ?");

        $stmt->bind_param("s", $name);

        $stmt->execute();

        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            // user existed
            $stmt->close();
            return true;
        } else {
            // user not existed
            $stmt->close();
            return false;
        }
    }


    /******* GAME *******/

    /**
     * Create Game
     */
    public function createGame($game_name,$owner,$number) {
        $stmt = $this->conn->prepare("INSERT INTO available_games_br(game_name,owner,number) VALUES(?,?,?);");
        $stmt->bind_param("sss",$game_name,$owner,$number);
        $result= $stmt->execute();
        $stmt->close();
        $game_data=array();
        $game_id="";

        if($result){

            $stmt = $this->conn->prepare("SELECT game_id FROM available_games_br WHERE game_name=?;");

            $stmt->bind_param("s", $game_name);
            $stmt->execute();
            $stmt->bind_result($game_id);

            while ($stmt->fetch()) {
                $game_data["game_id"]= $game_id;
            }

            if(empty($game_id)){
                $game_data["game_id"]=-1;
            }
            $stmt->close();
        }

        return $game_data;
    }

    /**
     * Check if Game exists or not
     */
    public function gameIsAvailableByName($game_name) {
        $stmt = $this->conn->prepare("SELECT game_id from available_games_br WHERE game_name = ?;");

        $stmt->bind_param("s", $game_name);
        $stmt->execute();

        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            // user existed
            $stmt->close();
            return false;
        } else {
            // user not existed
            $stmt->close();
            return true;
        }
    }

    /**
     * Check if Game exists or not
     */
    public function gameIsAvailableById($game_id) {
        $stmt = $this->conn->prepare("SELECT game_name from available_games_br WHERE game_id = ?;");

        $game_id=intval($game_id);
        $stmt->bind_param("i", $game_id);
        $stmt->execute();

        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            // user existed
            $stmt->close();
            return true;
        } else {
            // user not existed
            $stmt->close();
            return false;
        }
    }


    /**
     * Check if Game exists or not
     */
    public function gameIsReady($game_id) {
        $stmt = $this->conn->prepare("SELECT game_name from available_games_br WHERE game_id = ?;");

        $game_id=intval($game_id);
        $stmt->bind_param("i", $game_id);
        $stmt->execute();

        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            // user existed
            $stmt->close();
            return false;
        } else {
            $stmt->close();
    
            $stmt = $this->conn->prepare("SELECT game_name from games_br WHERE game_id = ? AND last_turn IS NULL;");
    
            $stmt->bind_param("i", $game_id);
            $stmt->execute();
    
            $stmt->store_result();
    
            if ($stmt->num_rows > 0) {
                // user existed
                $stmt->close();
                return true;
            }
            
            $stmt->close();
            return false;    
            
        }
    }

    /**
     * Check if Game is Ready or not
     */
    public function isGameOver($game_id) {
        $stmt = $this->conn->prepare("SELECT game_id from games_br WHERE game_id = ? AND winner IS NULL;");

        $stmt->bind_param("s", $game_id);
        $stmt->execute();

        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            // user existed
            $stmt->close();
            return false;
        } else {
            // user not existed
            $stmt->close();
            return true;
        }
    }

    /**
     * Return all waiting games
     */
    public function gamesWaiting() {

        $games_data=array();
        $stmt = $this->conn->prepare("SELECT game_id,game_name,owner FROM available_games_br;");
        $game_id=$game_name=$owner="";
        $stmt->execute();
        $stmt->bind_result($game_id,$game_name,$owner);
        $games=array();

        while ($stmt->fetch()) {
            $games_data["game_id"]=$game_id;
            $games_data["game_name"]=$game_name;
            $games_data["owner"]=$owner;
            $games[]=$games_data;
        }

        $stmt->close();

        return $games;
    }

    /**
     * Return data from an available game
     */
    public function gameData($game_id) {


        $stmt = $this->conn->prepare("SELECT * from available_games_br WHERE game_id = ?;");
        $game_id=intval($game_id);
        $stmt->bind_param("i", $game_id);
        $stmt->execute();
        $game_name=$player_1=$number_1=$last_turn="";
        $stmt->bind_result($game_id,$game_name,$player_1,$number_1);

        $game_data=array();

        while ($stmt->fetch()) {
            $game_data["game_id"]= $game_id;
            $game_data["game_name"]= $game_name;
            $game_data["player_1"]= $player_1;           
            $game_data["number_1"]=$number_1;
    
        }
        $stmt->close();

        return $game_data;
    }

    /**
     * Add player to a Game
     */
    public function addPlayer($game_id,$player_2,$number_player2) {

        $game_data=$this->gameData($game_id);
        $game_id=intval($game_id);
        if(!empty($game_data)){
          
            $stmt = $this->conn->prepare("INSERT INTO games_br(game_id,game_name,player_1,player_2,number_1,number_2,last_turn,winner) VALUES(?,?,?,?,?,?,NULL,NULL);");

            $name=$game_data["game_name"];
            $player_1=$game_data["player_1"];
            $number_1=$game_data["number_1"];
            $stmt->bind_param("isssss",$game_id,$name,$player_1,$player_2,$number_1,$number_player2);
                $result= $stmt->execute();
                $stmt->close();
                // check for successful store
            if ($result) {
            
                $stmt = $this->conn->prepare("DELETE FROM available_games_br WHERE game_id = ?;");
             
                $stmt->bind_param("i",$game_id);
                $result= $stmt->execute();
                $stmt->close();
                return true;
            } else {
                return false;
            }
            
        } else {
            return false;
        }

    }

    /**
     * Return data from an available game
     */
    public function lastPlayer($game_id) {

        $player="";
        $stmt = $this->conn->prepare("SELECT t.player from turns_br t, games_br g WHERE t.turn_id = g.last_turn AND g.game_id=?;");
        $game_id=intval($game_id);
        $stmt->bind_param("i", $game_id);
        $stmt->execute();
        $stmt->bind_result($player);
        $last_player="";

        while ($stmt->fetch()) {
            $last_player=$player;
        }
        $stmt->close();

        if(empty($last_player)){
            $stmt = $this->conn->prepare("SELECT player_2 from games_br WHERE game_id=?;");
            $stmt->bind_param("i", $game_id);
            $stmt->execute();
            $stmt->bind_result($player);
            $last_player="";
            while ($stmt->fetch()) {
                $last_player=$player;
            }
            $stmt->close();
        }

        return $last_player;
    }

    /**
     * Close the game
     */
    public function gameWinner($game_id,$winner) {
        $stmt = $this->conn->prepare("UPDATE games_br SET winner=? WHERE game_id = ?;");
        $game_id=intval($game_id);
        $stmt->bind_param("si",$winner,$game_id);
        $result= $stmt->execute();
        $stmt->close();
        // check for successful store
        if ($result) {
            $stmt = $this->conn->prepare("DELETE FROM turns_br WHERE game_id = ?;");
            $game_id=intval($game_id);
            $stmt->bind_param("i",$game_id);
            $stmt->execute();
            $stmt->close();
            return true;
        } else {
            return false;
        }
    }

    public function saveTurn($game_id, $player, $number, $corrects, $regulars)
    {

        if ($this->deleteTurns($game_id)) {

            $stmt = $this->conn->prepare("INSERT INTO turns_br(game_id,player,number,correct_numbers,regular_numbers) VALUES(?,?,?,?,?);");
            $game_id = intval($game_id);
            $stmt->bind_param("issii", $game_id, $player, $number, $corrects, $regulars);
            $result = $stmt->execute();
            $stmt->close();

            // check for successful store
            if ($result) {
                $turn_id=-1;
                $game_data=array();
                $stmt = $this->conn->prepare("SELECT turn_id FROM turns_br WHERE game_id=?;");

                $stmt->bind_param("i", $game_id);
                $stmt->execute();
                $stmt->bind_result($turn_id);

                while ($stmt->fetch()) {
                    $game_data["turn_id"] = $turn_id;
                }

                if($game_data["turn_id"]!=-1){
                    $this->saveLastTurn($game_id,$turn_id);
                }

                $stmt->close();
                return true;
            } else {
                return false;
            }

        }else{
            return false;
        }
    }

    private function deleteTurns($game_id){

        $stmt = $this->conn->prepare("DELETE FROM turns_br WHERE game_id = ?;");
        $game_id = intval($game_id);
        $stmt->bind_param("i", $game_id);
        $result= $stmt->execute();
        $stmt->close();

        // check for successful store
        if ($result) {
            return true;
        } else {
            return false;
        }

    }

    private function saveLastTurn($game_id,$turn_id){

        $stmt = $this->conn->prepare("UPDATE games_br SET last_turn=? WHERE game_id = ?;");
        $stmt->bind_param("ii",$turn_id,$game_id);
        $result= $stmt->execute();
        $stmt->close();
        // check for successful store
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function getLastTurn($game_id){

        $stmt = $this->conn->prepare("SELECT t.player,t.number,t.correct_numbers,t.regular_numbers from turns_br t, games_br g WHERE t.turn_id = g.last_turn AND g.game_id=?;");
        $game_id = intval($game_id);
        $stmt->bind_param("i", $game_id);
        $stmt->execute();
        $player = $number = $corrects = $regulars = "";
        $stmt->bind_result($player, $number, $corrects, $regulars);

        $game_data = array();

        while ($stmt->fetch()) {
            $game_data["player"] = $player;
            $game_data["number"] = $number;
            $game_data["corrects"] = $corrects;
            $game_data["regulars"] = $regulars;

        }
        
        $stmt->close();
        return $game_data;
    }

    public function getOpponentNumber($game_id,$player){

        $stmt = $this->conn->prepare("SELECT player_1,player_2,number_1,number_2 from games_br WHERE game_id = ?;");
        $game_id=intval($game_id);
        $stmt->bind_param("i", $game_id);
        $stmt->execute();
        $player_1=$player_2=$number_1=$number_2="";
        $stmt->bind_result($player_1,$player_2,$number_1,$number_2);

        $game_data=array();

        while ($stmt->fetch()) {
            $game_data["player_1"]= $player_1;
            $game_data["player_2"]=$player_2;
            $game_data["number_1"]=$number_1;
            $game_data["number_2"]=$number_2;
        }
        $stmt->close();

        if(strcmp($game_data["player_1"],$player)==0){
            return $game_data["number_2"];
        }else{
            return $game_data["number_1"];
        }

    }

}

?>