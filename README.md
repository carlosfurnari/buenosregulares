# README #

Buenos y Regulares es un juego de ingenio, donde se tratar� de descubir un numero oculto.

### What is this repository for? ###

* Este desarrollo corresponde al proyecto de la asignatura Software Libre y Desarrollo Social.

* En la carpeta server se encuentras los archivos php y las sentencias SQL para crear las bases de datos necesaria para correr la aplicaci�n y montar la API en un hosting.

* Version 1.3

### Next Versions ###

* Login de usuarios con mail y contrase�a.

* Mejoras en la interfaz gr�fica.

### How do I get set up? ###

* Download de APK

### Authors ###

* Ardizzone, Damian
* Furnari, Carlos

### License ###

* Apache License Version 2.0
* PHP