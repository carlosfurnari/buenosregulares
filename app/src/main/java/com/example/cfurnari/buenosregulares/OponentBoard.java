package com.example.cfurnari.buenosregulares;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;

public class OponentBoard extends AppCompatActivity {

    ArrayList<Arriesgada> entrada = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oponent_board);
        Bundle extras = getIntent().getExtras();
        if (extras != null)
        {
            entrada = (ArrayList<Arriesgada>)extras.getSerializable("arriesgada");
            View view = findViewById(R.id.clOponent);
            for (int i=0; i<entrada.size(); i++){
                Arriesgada elem = entrada.get(i);
                renderTable(view, elem.getCant(), elem.getNum(), elem.getBuenos(), elem.getRegulares());
            }

            TextView name = findViewById(R.id.tvUserName3);
            name.setText(extras.getString("player"));

        }
    }

    public void renderTable(View view, String cant, String number, String buenos, String regulares) {
        TableLayout table = findViewById(R.id.tlBoard);
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        LayoutInflater inflator = this.getLayoutInflater();
        TableRow rowView = new TableRow(view.getContext());
        inflator.inflate(R.layout.board_row, rowView);
        TextView tvCant = rowView.findViewById(R.id.tvCantI); // .
        tvCant.setText(cant);
        TextView tvNum = rowView.findViewById(R.id.tvNumberI); // .
        tvNum.setText(number);
        TextView buenos1 = rowView.findViewById(R.id.tvBuenosI);
        buenos1.setText(buenos);
        TextView regulares1 = rowView.findViewById(R.id.tvRegularesI);
        regulares1.setText(regulares);
        table.addView(rowView);
    }

    public void goBack(View view) {
        /*
        Intent intent = new Intent(this, MyBoardActivity.class);
        startActivity(intent);
        */
        onBackPressed();
    }
}
