package com.example.cfurnari.buenosregulares;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.example.cfurnari.buenosregulares.MainActivity.MyPREFERENCES;

/**
 * Created by dami on 09/12/17.
 */

public class PlayerBoard extends AppCompatActivity {

    public Integer attempts;
    SharedPreferences sharedpreferences;
    public ArrayList<Arriesgada> myList;
    public ArrayList<Arriesgada> opponentList;
    public boolean sigueJuego = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_board);

        if (savedInstanceState != null) {
            // por el momento no hace falta
        } else {
            attempts = 0;
            myList = new ArrayList<>();
            opponentList = new ArrayList<>();
            sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            String userName = sharedpreferences.getString("UserName", null);
            TextView tvUserName = (TextView) findViewById(R.id.tvUserName3);
            tvUserName.setText(userName);
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable("myList", myList);
        super.onSaveInstanceState(outState);
    }

    public void sendNumber(View view) {
        final EditText editText = findViewById(R.id.etNumbRisk2);
        final String riskedNumber = editText.getText().toString();
        final View actualView=view;
        String userName = sharedpreferences.getString("UserName", null);
        String gameId = sharedpreferences.getString("GameID", null);

        String URL="http://br.estudiorap.com.ar/game.php?action=sendNumber&user="+userName+"&game="+gameId+"&number="+riskedNumber;

        if(!riskedNumber.isEmpty()){
            RequestQueue queue= Volley.newRequestQueue(this);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    URL, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("NumberRisked",response.toString());
                    String status="";


                    try {
                        status = response.getString("status");
                        if(status.equals("Ok")){
                            Integer correctValues = response.getInt("correct");
                            Integer regularValues = response.getInt("regular");
                            if (correctValues == 4) {
                                RelativeLayout popUp = findViewById(R.id.rlPopUp);
                                popUp.setVisibility(View.VISIBLE);
                            } else {
                                Arriesgada save = new Arriesgada(attempts.toString(), riskedNumber, correctValues.toString(), regularValues.toString());
                                myList.add(save);
                                renderTable(actualView, attempts.toString(), riskedNumber, correctValues.toString(), regularValues.toString());
                            }
                            editText.setText("");

                            oponentPlays(actualView);
                        }else{
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("User","error");
                }
            });

            queue.add(jsonObjReq);

        }else{
            //TODO: handle error
        }

    }


    public void renderTable(View view, String cant, String number, String buenos, String regulares) {
        TableLayout table = findViewById(R.id.tlBoard);
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        LayoutInflater inflator = this.getLayoutInflater();
        TableRow rowView = new TableRow(view.getContext());
        inflator.inflate(R.layout.board_row, rowView);
        TextView tvCant = rowView.findViewById(R.id.tvCantI); // .
        tvCant.setText(cant);
        TextView tvNum = rowView.findViewById(R.id.tvNumberI); // .
        tvNum.setText(number);
        TextView buenos1 = rowView.findViewById(R.id.tvBuenosI);
        buenos1.setText(buenos);
        TextView regulares1 = rowView.findViewById(R.id.tvRegularesI);
        regulares1.setText(regulares);
        table.addView(rowView);
    }



    public void goMenu(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void seeOponent(View view) {
        Intent intent = new Intent(this, OponentBoard.class);
        intent.putExtra("arriesgada", opponentList);
        startActivity(intent);
    }

    public void oponentPlays(View view) {


        final View actualView=view;
        String userName = sharedpreferences.getString("UserName", null);
        String gameId = sharedpreferences.getString("GameID", null);

        String URL="http://br.estudiorap.com.ar/game.php?action=waitOpponentNumber&user="+userName+"&game="+gameId;

            RequestQueue queue= Volley.newRequestQueue(this);
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    URL, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("NumberRisked",response.toString());
                    String status="";

                    try {
                        status = response.getString("status");
                        if(status.equals("Ok")){
                            Integer correctValues = response.getInt("correct");
                            Integer regularValues = response.getInt("regular");
                            String riskedNumber = response.getString("riskedNumber");
                            if (correctValues == 4) {
                                RelativeLayout popUp = findViewById(R.id.rlPopUp);
                                TextView result = findViewById(R.id.tvGameOver);
                                result.setText("PERDISTE !");
                                popUp.setBackgroundColor(Color.RED);
                                popUp.setVisibility(View.VISIBLE);
                            } else {
                                Arriesgada save = new Arriesgada(attempts.toString(), riskedNumber, correctValues.toString(), regularValues.toString());
                                opponentList.add(save);
                                renderTable(actualView, attempts.toString(), riskedNumber, correctValues.toString(), regularValues.toString());
                            }

                        }else {
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("User","error");
                }
            });

            queue.add(jsonObjReq);

    }

}
