package com.example.cfurnari.buenosregulares;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.example.cfurnari.buenosregulares.MainActivity.MyPREFERENCES;

public class ChooseNumberActivity extends AppCompatActivity {

    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_number);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        String mode = sharedpreferences.getString("PlayMode", null);

        if(mode.equals("PVP-Create")){
            TextView tvCreate = findViewById(R.id.tvCreateName);
            tvCreate.setVisibility(View.VISIBLE);
            EditText etCreate = findViewById(R.id.etCreateName);
            etCreate.setVisibility(View.VISIBLE);
        }
    }

    public void increaseNum1(View view) {
        TextView textView = findViewById(R.id.tvNum1);
        increaseNum(textView);
    }

    public void increaseNum2(View view) {
        TextView textView = findViewById(R.id.tvNum2);
        increaseNum(textView);
    }

    public void increaseNum3(View view) {
        TextView textView = findViewById(R.id.tvNum3);
        increaseNum(textView);
    }

    public void increaseNum4(View view) {
        TextView textView = findViewById(R.id.tvNum4);
        increaseNum(textView);
    }

    public void increaseNum(TextView textView) {
        String aux = textView.getText().toString();
        Integer num = Integer.parseInt(aux);
        num++;
        if (num == 10) {
            num = 0;
        }
        textView.setText(num.toString());
    }

    public void decreaseNum1(View view) {
        TextView textView = findViewById(R.id.tvNum1);
        decreaseNum(textView);
    }

    public void decreaseNum2(View view) {
        TextView textView = findViewById(R.id.tvNum2);
        decreaseNum(textView);
    }

    public void decreaseNum3(View view) {
        TextView textView = findViewById(R.id.tvNum3);
        decreaseNum(textView);
    }

    public void decreaseNum4(View view) {
        TextView textView = findViewById(R.id.tvNum4);
        decreaseNum(textView);
    }
    public void decreaseNum(TextView textView) {
        String aux = textView.getText().toString();
        Integer num = Integer.parseInt(aux);
        num--;
        if (num < 0) {
            num = 9;
        }
        textView.setText(num.toString());
    }

    public void ready(View view) {
        TextView error = findViewById(R.id.tvErrorMsg);
        TextView textView = findViewById(R.id.tvNum1);
        String aux = textView.
                getText().toString();
        textView = findViewById(R.id.tvNum2);
        aux += textView.getText().toString();
        textView = findViewById(R.id.tvNum3);
        aux += textView.getText().toString();
        textView = findViewById(R.id.tvNum4);
        aux += textView.getText().toString();
        if (isValid(aux)) {
            error.setVisibility(View.INVISIBLE);
            this.saveNumber(aux, view);
        } else {
            error.setVisibility(View.VISIBLE);
        }

    }

    public boolean isValid(String input) {
        Integer containsUnique = 0;
        for (int i = 0; i<4; i++) {
            for (int j = 0; j<4; j++) {
                if (i != j && (input.charAt(i) == input.charAt(j))) {
                    containsUnique++;
                }
            }
        }
        if (containsUnique > 0) {
            return false;
        } else {
            return true;
        }
    }

    private void saveNumber(String number, View view){

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("SelectedNumber", number);
        editor.commit();
        String mode = sharedpreferences.getString("PlayMode", null);
        Log.d("Mode ", mode);
        String owner = sharedpreferences.getString("UserName", null);
        Log.d("Owner ", owner);
        Intent intent = null;
        if (mode.equals("PVP-Create")) {
            Log.d("PVP-Create ", mode);
            EditText name = findViewById(R.id.etCreateName);
            String gameName = name.getText().toString();
            String formatedNumber = number.charAt(0) + "," + number.charAt(1) + "," + number.charAt(2) + "," + number.charAt(3);
            sendCreate(owner, gameName, formatedNumber);
            intent = new Intent(this, RoomActivity.class);
        } else if (mode.equals("PVP-Join")) {
            Log.d("PVP-Join ", mode);
            String id = sharedpreferences.getString("GameId", null);
            String formatedNumber = number.charAt(0) + "," + number.charAt(1) + "," + number.charAt(2) + "," + number.charAt(3);
            sendJoin(id, owner, formatedNumber);
            intent = new Intent(this, MyBoardPVP.class);
        } else {
            Log.d("Else ", mode);
            intent = new Intent(this, MyBoardActivity.class);
        }
        startActivity(intent);
    }

    private void sendCreate(String owner, String name, String number) {

        String URL = "http://br.estudiorap.com.ar/game.php?action=createGame&owner=" + owner + "&game_name=" + name + "&number=" + number;
        RequestQueue queue= Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("Create Game",response.toString());
                String status="";
                try {
                    status = response.getString("status");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(status.equals("Ok")){
                    try {
                        String gameId  = response.getString("game_id");
                        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString("GameId", gameId);
                        editor.putString("CreatedGameId", gameId);
                        editor.commit();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    //
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("User","error");
            }
        });

        queue.add(jsonObjReq);
    }

    private void sendJoin(String id, String player, String number) {

        String URL = "http://br.estudiorap.com.ar/game.php?action=joinGame&game_id=" + id + "&player=" + player + "&number=" + number;
        Log.d("Join Game Uri", URL);
        RequestQueue queue= Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("Join Game",response.toString());
                String status="";
                try {
                    status = response.getString("status");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(status.equals("Ok")){
                    //
                }else{
                    //
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("User","error");
            }
        });

        queue.add(jsonObjReq);
    }
}
