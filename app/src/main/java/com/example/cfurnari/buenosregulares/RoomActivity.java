package com.example.cfurnari.buenosregulares;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import static com.example.cfurnari.buenosregulares.MainActivity.MyPREFERENCES;

public class RoomActivity extends AppCompatActivity {

    public String gameId = "";
    SharedPreferences sharedpreferences;
    private Timer scheduledTasks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("GameId", "");
        editor.commit();

        Button btJoin = findViewById(R.id.btJoin);
        btJoin.setEnabled(false);
        scheduledTasks = new Timer();

        getCurrentGames();
        scheduledTasks.scheduleAtFixedRate(new TimerTask() {

                                  @Override
                                  public void run() {

                                      getCurrentGames();
                                      gameIsReady();
                                  }

                              },
//Set how long before to start calling the TimerTask (in milliseconds)
                0,
//Set the amount of time between each execution (in milliseconds)
                5000);
        scheduledTasks.scheduleAtFixedRate(new TimerTask() {

                                             @Override
                                             public void run() {

                                             }

                                         },0, 5000);

    }
/*
    @Override
    protected void onResume() {
        super.onResume();
        refreshTable = new Timer();
        refreshTable.scheduleAtFixedRate(new TimerTask() {

                                              @Override
                                              public void run() {
                                                  getCurrentGames();
                                              }

                                          },
//Set how long before to start calling the TimerTask (in milliseconds)
                0,
//Set the amount of time between each execution (in milliseconds)
                5000);
        getCurrentGames();
    }

    @Override
    protected void onPause() {
        super.onPause();
        refreshTable.cancel();
    }*/

    private void gameIsReady() {


        RequestQueue queue= Volley.newRequestQueue(this);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        String createdGame = sharedpreferences.getString("GameId", null);
        String URL="http://br.estudiorap.com.ar/game.php?action=isGameReady&game_id="+createdGame;
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Log.d("Game is Ready",response.toString());
                String status="";
                Boolean ready = false;
                try {
                    status = response.getString("status");
                    ready = response.getBoolean("ready");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(status.equals("Ok")){
                    if(ready){
                        Intent intent = new Intent(RoomActivity.this, MyBoardPVP.class);
                        scheduledTasks.cancel();
                        startActivity(intent);
                    }
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("User","error");
            }
        });

        queue.add(jsonObjReq);
    }

    private void getCurrentGames() {

        RequestQueue queue= Volley.newRequestQueue(this);
        String URL="http://br.estudiorap.com.ar/game.php?action=getGames";
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("Get Games",response.toString());
                String status="";
                JSONArray games = null;
                try {
                    status = response.getString("status");
                    games = response.getJSONArray("games");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(status.equals("Ok")){
                    try {
                        showGames(games);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    //
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("User","error");
            }
        });

        queue.add(jsonObjReq);
    }

    public void showGames(JSONArray games) throws JSONException {
        //Log.d("Show Games",games.toString());
        View view = findViewById(R.id.clRoom);
        final TableLayout table = findViewById(R.id.tlGames);
        while(table.getChildCount() > 1) {
            table.removeViewAt(1);
        }
        for (int j=0; j < games.length(); j++){
            JSONObject item = games.getJSONObject(j);
            renderRoomTable(view, item.getString("game_id"), item.getString("game_name"), item.getString("owner"));
        }
    }

    public void renderRoomTable(View view, final String id, String partida, String owner) {
        Log.d("Render Games", id + " " + partida + " " + owner);
        final TableLayout table = findViewById(R.id.tlGames);

        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        LayoutInflater inflator = this.getLayoutInflater();
        final TableRow rowView = new TableRow(view.getContext());
        inflator.inflate(R.layout.game_row, rowView);
        TextView tvNumb = rowView.findViewById(R.id.tvIdI); // .
        tvNumb.setText(id);
        TextView tvName = rowView.findViewById(R.id.tvNameI); // .
        tvName.setText(partida);
        TextView tvOwner = rowView.findViewById(R.id.tvOwnerI); // .
        tvOwner.setText(owner);
        rowView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                for (int i = 0; i < table.getChildCount(); i++) {
                    table.getChildAt(i).setBackgroundColor(Color.TRANSPARENT);
                }
                view.setBackgroundColor(Color.LTGRAY);
                gameId = ((TextView)rowView.findViewById(R.id.tvIdI)).getText().toString();
                Log.d("game id ", gameId);
                String createdGameId = sharedpreferences.getString("GameId", null);
                if(!gameId.equals(createdGameId)){
                    sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("GameId", gameId);
                    editor.commit();
                    Button btJoin = findViewById(R.id.btJoin);
                    btJoin.setEnabled(true);
                }

            }
        });
        table.addView(rowView);
    }

    public void newGamePlay(View view) {
        Intent intent = new Intent(this, ChooseNumberActivity.class);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("PlayMode", "PVP-Create");
        editor.commit();
        scheduledTasks.cancel();
        Button btCreate = findViewById(R.id.btCreate);
        btCreate.setEnabled(false);
        startActivity(intent);
    }

    public void joinGame(View view) {
        Intent intent = new Intent(this, ChooseNumberActivity.class);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("PlayMode", "PVP-Join");
        editor.commit();
        scheduledTasks.cancel();
        startActivity(intent);
    }
}
