package com.example.cfurnari.buenosregulares;

import java.io.Serializable;

/**
 * Created by Samsung on 06/12/2017.
 */

public class Arriesgada implements Serializable {

    public String cant;
    public String num;
    public String buenos;
    public String regulares;

    public Arriesgada(String icant, String inum, String ibuenos, String iregulares) {
        cant = icant;
        num = inum;
        buenos = ibuenos;
        regulares = iregulares;
    }

    public String getCant() {
        return cant;
    }

    public String getNum() {
        return num;
    }

    public String getBuenos() {
        return buenos;
    }

    public String getRegulares() {
        return regulares;
    }

    public void setCant(String cant) {
        this.cant = cant;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public void setBuenos(String buenos) {
        this.buenos = buenos;
    }

    public void setRegulares(String regulares) {
        this.regulares = regulares;
    }
}
