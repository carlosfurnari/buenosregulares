package com.example.cfurnari.buenosregulares;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends AppCompatActivity {

    public static final String MyPREFERENCES = "MyPrefs" ;
    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button  btnLogin = (Button)findViewById(R.id.btLogin);



        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText userText = (EditText) findViewById(R.id.etPlayerName);

                String userName=userText.getText().toString();

                if (userName.isEmpty()) {
                    Toast.makeText(getApplicationContext(),
                            "Complete todos los campos", Toast.LENGTH_LONG)
                            .show();
                } else {
                    userLogin("http://br.estudiorap.com.ar/login.php?user="+userName);
                }

            }
        });

    }

    /** Called when the user taps the Send button */
    public void setUser(String userName) {

        Intent intent = new Intent(MainActivity.this, StartGameActivity.class);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("UserName", userName);
        editor.commit();
        startActivity(intent);
    }


    private void userLogin(String URL){

        RequestQueue queue= Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("Login",response.toString());
                String status="";
                String userName="";

                try {
                    status = response.getString("status");
                    userName = response.getString("user");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(status.equals("Ok")){
                    setUser(userName);
                }else{

                    Toast.makeText(getApplicationContext(),
                            "El usuario ya existe", Toast.LENGTH_LONG)
                            .show();
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("User","error");
            }
        });

        queue.add(jsonObjReq);
    }

}
