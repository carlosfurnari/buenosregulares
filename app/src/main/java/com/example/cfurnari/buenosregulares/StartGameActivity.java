package com.example.cfurnari.buenosregulares;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import static com.example.cfurnari.buenosregulares.MainActivity.MyPREFERENCES;

public class StartGameActivity extends AppCompatActivity {

    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_game);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        String userName = sharedpreferences.getString("UserName", null);
        TextView tvPlayer = (TextView) findViewById(R.id.tvUserName3);
        tvPlayer.setText(userName);

    }

    public void playMachine(View view) {
        Intent intent = new Intent(this, ChooseNumberActivity.class);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("PlayMode", "Machine");
        editor.commit();
        startActivity(intent);
    }

    public void playPvsP(View view) {
        Intent intent = new Intent(this, RoomActivity.class);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("PlayMode", "PvsP");
        editor.commit();
        startActivity(intent);
    }
}
