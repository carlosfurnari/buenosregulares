package com.example.cfurnari.buenosregulares;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import static com.example.cfurnari.buenosregulares.MainActivity.MyPREFERENCES;

public class MyBoardPVP extends AppCompatActivity {
    public Integer attempts;
    SharedPreferences sharedpreferences;
    public ArrayList<Arriesgada> myList;
    public ArrayList<Arriesgada> oponentList;
    public boolean myTurn = false;
    public boolean sigueJuego = true;
    private Timer scheduledTask;
    public String oPlayer = "";
    public String playerNumb = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_board_pvp);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        if (savedInstanceState != null) {
            // por el momento no hace falta
        } else {
            attempts = 0;
            myList = new ArrayList<>();
            oponentList = new ArrayList<>();
            sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            final String userName = sharedpreferences.getString("UserName", null);
            TextView tvUserName = (TextView) findViewById(R.id.tvUserName3);
            tvUserName.setText(userName);

            scheduledTask = new Timer();
            scheduledTask.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    //Log.d("mi turno ", String.valueOf(myTurn));
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            checkTurn(sharedpreferences.getString("GameId", null), userName);
                            // This code will always run on the UI thread, therefore is safe to modify UI elements.
                            if (!myTurn) {
                                waitPlayer();
                                /*
                                RelativeLayout popUp = findViewById(R.id.rlPopUp2);
                                popUp.setVisibility(View.VISIBLE);
                                popUp.setBackgroundColor(Color.LTGRAY);
                                TextView msg = findViewById(R.id.tvGameMsg2);
                                msg.setText("Espere por su turno");
                                */
                            } else {
                                iPlay();
                                /*
                                RelativeLayout popUp = findViewById(R.id.rlPopUp2);
                                popUp.setVisibility(View.INVISIBLE);
                                */
                            }
                        }
                    });
                }
                },
    //Set how long before to start calling the TimerTask (in milliseconds)
                    0,
    //Set the amount of time between each execution (in milliseconds)
                    1000);

        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable("myList", myList);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        super.onSaveInstanceState(outState);
    }


    public boolean isValid(String input) {
        Integer containsUnique = 0;
        for (int i = 0; i<4; i++) {
            for (int j = 0; j<4; j++) {
                if (i != j && (input.charAt(i) == input.charAt(j))) {
                    containsUnique++;
                }
            }
        }
        if (containsUnique > 0) {
            return false;
        } else {
            return true;
        }
    }


    public void sendNumber(View view) {
        EditText editText = findViewById(R.id.etNumbRisk2);
        String aux = editText.getText().toString();
        if (!aux.isEmpty() && this.isValid(aux)) {
            attempts++;
            String userName = sharedpreferences.getString("UserName", null);
            String gameId = sharedpreferences.getString("GameId", null);
            String riskedNumber = aux.charAt(0) + "," + aux.charAt(1) + "," + aux.charAt(2) + "," + aux.charAt(3);
            Button send = findViewById(R.id.btSend2);
            send.setEnabled(false);
            callServer(view, userName, gameId, riskedNumber);
            editText.setText("");
        }else{
            Toast.makeText(getApplicationContext(),
                    "Ingrese un número válido. Sin números repetidos.", Toast.LENGTH_LONG)
                    .show();
        }
    }

    public void callServer(final View view, String userName, String gameId, final String inRisked) {
        String URL="http://br.estudiorap.com.ar/game.php?action=sendNumber&player="+userName+"&game_id="+gameId+"&number="+inRisked;
        //Log.d("uri: ", URL);
        RequestQueue queue= Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    URL, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    //Log.d("NumberRisked",response.toString());
                    String status="";

                    try {
                        status = response.getString("status");
                        if(status.equals("Ok")){
                            Integer correctValues = response.getInt("corrects");
                            Integer regularValues = response.getInt("regulars");
                            if (correctValues == 4) {
                                iWin();
                                scheduledTask.cancel();
                            } else {
                                String auxx = String.valueOf(inRisked.charAt(0)) + String.valueOf(inRisked.charAt(2)) +
                                                String.valueOf(inRisked.charAt(4)) + String.valueOf(inRisked.charAt(6)) ;
                                Arriesgada save = new Arriesgada(attempts.toString(), auxx, correctValues.toString(), regularValues.toString());
                                myList.add(save);
                                renderTable(view, attempts.toString(), auxx, correctValues.toString(), regularValues.toString());
                            }
                        }else{

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("User","error");
                }
            });

        queue.add(jsonObjReq);
    }

    public void checkTurn (String gameId, final String player) {

        String URL = "http://br.estudiorap.com.ar/game.php?action=isMyTurn&game_id=" + gameId + "&player=" + player;
        RequestQueue queue= Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Log.d("Get Games",response.toString());
                String status="";
                try {
                    status = response.getString("status");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(status.equals("Ok")){
                    try {
                        myTurn = false;
                        Boolean turn = response.getBoolean("turn");
                        if (turn) {
                            myTurn = true;
                            String oNumb = response.getString("number");
                            String auxx = String.valueOf(oNumb.charAt(0)) + String.valueOf(oNumb.charAt(2)) +
                                    String.valueOf(oNumb.charAt(4)) + String.valueOf(oNumb.charAt(6)) ;
                            if (!auxx.equals(playerNumb)){
                                playerNumb = auxx;
                                String oBuenos = response.getString("corrects");
                                String oPlay = response.getString("player");
                                oPlayer = oPlay;
                                String oRegulares = response.getString("regulars");
                                if (oBuenos.equals("null")) {
                                    oBuenos = "0";
                                }
                                if (oRegulares.equals("null")) {
                                    oRegulares = "0";
                                }
                                Arriesgada save = new Arriesgada(attempts.toString(), auxx, oBuenos, oRegulares);
                                oponentList.add(save);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else if (status.equals("Fin")){
                    gameOver();
                    scheduledTask.cancel();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("User","error");
            }
        });

        queue.add(jsonObjReq);
    }

    public void renderTable(View view, String cant, String number, String buenos, String regulares) {
        TableLayout table = findViewById(R.id.tlBoard2);
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        LayoutInflater inflator = this.getLayoutInflater();
        TableRow rowView = new TableRow(view.getContext());
        inflator.inflate(R.layout.board_row, rowView);
        TextView tvCant = rowView.findViewById(R.id.tvCantI); // .
        tvCant.setText(cant);
        TextView tvNum = rowView.findViewById(R.id.tvNumberI); // .
        tvNum.setText(number);
        TextView buenos1 = rowView.findViewById(R.id.tvBuenosI);
        if (buenos.equals("null")) {
            buenos = "0";
        }
        buenos1.setText(buenos);
        TextView regulares1 = rowView.findViewById(R.id.tvRegularesI);
        if (regulares.equals("null")) {
            regulares = "0";
        }
        regulares1.setText(regulares);
        table.addView(rowView);
    }

    public void goMenu(View view) {
        Intent intent = new Intent(this, StartGameActivity.class);
        startActivity(intent);
    }

    public void seeOponent(View view) {
        Intent intent = new Intent(this, OponentBoard.class);
        intent.putExtra("arriesgada", oponentList);
        intent.putExtra("player", oPlayer);
        startActivity(intent);
    }

    public void waitPlayer(){
        RelativeLayout popUp = findViewById(R.id.rlPopUp2);
        popUp.setVisibility(View.VISIBLE);
        popUp.setBackgroundColor(Color.LTGRAY);
        TextView msg = findViewById(R.id.tvGameMsg2);
        msg.setText("Espere por su turno");
        Button send = findViewById(R.id.btSend2);
        send.setEnabled(false);
    }

    public void iPlay(){
        RelativeLayout popUp = findViewById(R.id.rlPopUp2);
        popUp.setVisibility(View.INVISIBLE);
        Button send = findViewById(R.id.btSend2);
        send.setEnabled(true);
    }

    public void gameOver() {
        RelativeLayout popUp = findViewById(R.id.rlPopUp2);
        TextView result = findViewById(R.id.tvGameMsg2);
        result.setText("PERDISTE !");
        popUp.setBackgroundColor(Color.RED);
        popUp.setVisibility(View.VISIBLE);
        Button send = findViewById(R.id.btSend2);
        send.setEnabled(false);
        Button back= findViewById(R.id.btMenu2);
        back.setVisibility(View.VISIBLE);
        back.setEnabled(true);
    }

    public void iWin() {
        RelativeLayout popUp = findViewById(R.id.rlPopUp2);
        TextView result = findViewById(R.id.tvGameMsg2);
        result.setText("GANASTE !!");
        popUp.setBackgroundColor(Color.GREEN);
        popUp.setVisibility(View.VISIBLE);
        Button enviar = findViewById(R.id.btSend2);
        enviar.setEnabled(false);
        Button back= findViewById(R.id.btMenu2);
        back.setVisibility(View.VISIBLE);
        back.setEnabled(true);

    }
}
