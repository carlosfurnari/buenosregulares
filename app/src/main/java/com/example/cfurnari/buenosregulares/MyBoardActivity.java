package com.example.cfurnari.buenosregulares;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static com.example.cfurnari.buenosregulares.MainActivity.MyPREFERENCES;

public class MyBoardActivity extends AppCompatActivity {
    public Integer intents;
    private Integer[] secretNumber;
    private Integer[] myNumberList;
    private String myNumber;
    SharedPreferences sharedpreferences;
    public ArrayList<Arriesgada> myList;
    public ArrayList<Arriesgada> pcList;
    public List<Integer> allNumList;
    public List<Integer> correctNumbers;
    public List<Integer> lastNumber;
    public List<Integer> ind;
    public boolean sigueJuego = true;
    public boolean primeraParte = true;
    public boolean hardMode = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_board);

        if (savedInstanceState != null) {
            // por el momento no hace falta
        } else {
            intents = 0;
            secretNumber = getSecretNumber();
            myList = new ArrayList<>();
            pcList = new ArrayList<>();
            sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            myNumber = sharedpreferences.getString("SelectedNumber", null);
            myNumberList = new Integer[4];
            myNumberList[0] = (Integer.valueOf(myNumber.charAt(0)) - 48);
            myNumberList[1] = (Integer.valueOf(myNumber.charAt(1)) - 48);
            myNumberList[2] = (Integer.valueOf(myNumber.charAt(2)) - 48);
            myNumberList[3] = (Integer.valueOf(myNumber.charAt(3)) - 48);
            correctNumbers = new ArrayList<>();
            allNumList = new ArrayList<>();
            lastNumber = new ArrayList<>();
            lastNumber.add(0,1);
            lastNumber.add(1,1);
            lastNumber.add(2,1);
            lastNumber.add(3,1);
            ind = new ArrayList<>();
            for (int i=0; i<10; i++){
                allNumList.add(i);
            }
            String userName = sharedpreferences.getString("UserName", null);
            TextView tvUserName = (TextView) findViewById(R.id.tvUserName3);
            tvUserName.setText(userName);
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable("myList", myList);
        super.onSaveInstanceState(outState);
    }

    public void sendNumber(View view) {
        EditText editText = findViewById(R.id.etNumbRisk2);
        String aux = editText.getText().toString();
        if (!aux.isEmpty()) {
            intents++;

            Integer cantBuenos = calculateBuenos(aux, secretNumber).size();
            if (cantBuenos == 4) {
                RelativeLayout popUp = findViewById(R.id.rlPopUp);
                popUp.setVisibility(View.VISIBLE);
            } else {
                Integer cantRegulares = calculateRegulares(aux, secretNumber).size();
                Arriesgada save = new Arriesgada(intents.toString(), aux, cantBuenos.toString(), cantRegulares.toString());
                myList.add(save);
                renderTable(view, intents.toString(), aux, cantBuenos.toString(), cantRegulares.toString());
            }
            editText.setText("");
            pcPlays();
            if (sigueJuego == false) {
                RelativeLayout popUp = findViewById(R.id.rlPopUp);
                TextView result = findViewById(R.id.tvGameOver);
                result.setText("PERDISTE !");
                popUp.setBackgroundColor(Color.RED);
                popUp.setVisibility(View.VISIBLE);
            }
        }
    }

    public void renderTable(View view, String cant, String number, String buenos, String regulares) {
        TableLayout table = findViewById(R.id.tlBoard);
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        LayoutInflater inflator = this.getLayoutInflater();
        TableRow rowView = new TableRow(view.getContext());
        inflator.inflate(R.layout.board_row, rowView);
        TextView tvCant = rowView.findViewById(R.id.tvCantI); // .
        tvCant.setText(cant);
        TextView tvNum = rowView.findViewById(R.id.tvNumberI); // .
        tvNum.setText(number);
        TextView buenos1 = rowView.findViewById(R.id.tvBuenosI);
        buenos1.setText(buenos);
        TextView regulares1 = rowView.findViewById(R.id.tvRegularesI);
        regulares1.setText(regulares);
        table.addView(rowView);
    }

    public Integer[] getSecretNumber() {
        Random randomGenerator = new Random();
        Integer[] result = new Integer[4];
        result[0] = randomGenerator.nextInt(10);
        result[1] = randomGenerator.nextInt(10);
            while (result[0] == result[1]) {
                result[1] = randomGenerator.nextInt(10);
        }
        result[2] = randomGenerator.nextInt(10);
        while (result[2] == result[0] || result[2] == result[1]) {
            result[2] = randomGenerator.nextInt(10);
        }
        result[3] = randomGenerator.nextInt(10);
        while (result[3] == result[0] || result[3] == result[1] || result[3] == result[2]) {
            result[3] = randomGenerator.nextInt(10);
        }
        return result;
    }

    public HashMap<Integer,Integer> calculateBuenos(String input, Integer[] number) {
        HashMap<Integer,Integer> result = new HashMap<>();
        for (int i=0; i<number.length; i++) {
            if (number[i] == (Integer.valueOf(input.charAt(i)) - 48)) {
                result.put(i, number[i]);
            }
        }
        return result;
    }

    public HashMap<Integer,Integer> calculateRegulares(String input, Integer[] number) {
        HashMap<Integer,Integer> result = new HashMap<>();
        for (int i=0; i<number.length; i++) {
            for (int j=0; j<4; j++) {
                if ((number[i] == (Integer.valueOf(input.charAt(j))) - 48) && (i!=j)) {
                    result.put(i, number[i]);
                }
            }
        }
        return result;
    }

    public void goMenu(View view) {
        Intent intent = new Intent(this, StartGameActivity.class);
        startActivity(intent);
    }

    public void seeOponent(View view) {
        Intent intent = new Intent(this, OponentBoard.class);
        intent.putExtra("arriesgada", pcList);
        intent.putExtra("player", "MacGyver");
        startActivity(intent);
    }

    public void pcPlays() {
        if (primeraParte) {
            String num = calculatePcNumber();
            HashMap<Integer,Integer> buenos = calculateBuenos(num, myNumberList);
            if (buenos.size() == 4){
                sigueJuego = false;
            }
            HashMap<Integer,Integer> regulares = calculateRegulares(num, myNumberList);
            for(Map.Entry m:buenos.entrySet()){
                int pos = allNumList.indexOf(m.getValue());
                if (pos != -1){
                    correctNumbers.add((Integer)m.getValue());
                    allNumList.remove(pos);
                }
            }
            if (hardMode){
                for(Map.Entry m:regulares.entrySet()){
                    int pos = allNumList.indexOf(m.getValue());
                    if (pos != -1){
                        correctNumbers.add((Integer)m.getValue());
                        allNumList.remove(pos);
                    }
                }
            }
            Arriesgada arri = new Arriesgada(intents.toString(), num, String.valueOf(buenos.size()), String.valueOf(regulares.size()));
            pcList.add(arri);
            if (correctNumbers.size() == 4 ){
                primeraParte = false;
            }
        } else {
            calculatePcNumber2();
            String num = String.valueOf(lastNumber.get(0)) + String.valueOf(lastNumber.get(1)) +
                    String.valueOf(lastNumber.get(2)) + String.valueOf(lastNumber.get(3));
            HashMap<Integer,Integer> buenos = calculateBuenos(num, myNumberList);
            if (buenos.size() == 4){
                sigueJuego = false;
            }
            HashMap<Integer,Integer> regulares = calculateRegulares(num, myNumberList);
            for(Map.Entry m:buenos.entrySet()){
                int pos = correctNumbers.indexOf(m.getValue());
                if (pos != -1){
                    lastNumber.set((Integer)m.getKey(), (Integer)m.getValue());
                    ind.add((Integer) m.getKey());
                    correctNumbers.remove(pos);
                }
            }
            Arriesgada arri = new Arriesgada(intents.toString(), num, String.valueOf(buenos.size()), String.valueOf(regulares.size()));
            pcList.add(arri);
        }
    }

    public String calculatePcNumber(){
        Random rand = new Random();
        List<Integer> actual = new ArrayList<>(allNumList);
        int randomElement1 = actual.remove(rand.nextInt(actual.size()));
        int randomElement2 = actual.remove(rand.nextInt(actual.size()));
        int randomElement3 = actual.remove(rand.nextInt(actual.size()));
        int randomElement4 = actual.remove(rand.nextInt(actual.size()));
        String num = String.valueOf(randomElement1) + String.valueOf(randomElement2) +
                String.valueOf(randomElement3) + String.valueOf(randomElement4);
        return num;
    }

    public void calculatePcNumber2(){
        Random rand = new Random();
        List<Integer> actual = new ArrayList<>(correctNumbers);
        for (int i=0; i<4; i++) {
            if (ind.indexOf(i) == -1) {
                lastNumber.set(i,actual.remove(rand.nextInt(actual.size())));
            }
        }
        if (yaExiste()) {
            calculatePcNumber2();
        }
    }

    public boolean yaExiste(){
        boolean result = false;
        String num = String.valueOf(lastNumber.get(0)) + String.valueOf(lastNumber.get(1)) +
                String.valueOf(lastNumber.get(2)) + String.valueOf(lastNumber.get(3));
        for (int i=0; i<pcList.size();i++){
            Arriesgada aux = pcList.get(i);
            if (num.equals(aux.getNum())){
                result = true;
            }
        }
        return result;
    }
}
